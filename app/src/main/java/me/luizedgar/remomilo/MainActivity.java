package me.luizedgar.remomilo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.luizedgar.remomilo.data.Resource;
import me.luizedgar.remomilo.data.ResourcesRepository;
import me.luizedgar.remomilo.data.local.RemoMiloDatabase;
import me.luizedgar.remomilo.data.local.ResourcesLocalDataSource;
import me.luizedgar.remomilo.data.remote.Api;
import me.luizedgar.remomilo.resources.ResourcesFragment;
import me.luizedgar.remomilo.resources.ResourcesPresenter;
import me.luizedgar.remomilo.utils.ActivityUtils;
import me.luizedgar.remomilo.utils.AppExecutors;

public class MainActivity extends AppCompatActivity implements ResourcesFragment.OnListFragmentInteractionListener {

    ResourcesPresenter mResourcesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ResourcesFragment resourcesFragment =
                (ResourcesFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (resourcesFragment == null) {
            resourcesFragment = ResourcesFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), resourcesFragment, R.id.contentFrame);
        }

        mResourcesPresenter = new ResourcesPresenter(resourcesFragment, ResourcesRepository.getInstance(Api.getClient(), ResourcesLocalDataSource.getInstance(new AppExecutors(), RemoMiloDatabase.getInstance(this).resourceDao())));

        if (savedInstanceState != null) {

        }

    }

    @Override
    public void onListFragmentInteraction(Resource resource) {

    }

}
