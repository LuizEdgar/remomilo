package me.luizedgar.remomilo.resources;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.luizedgar.remomilo.R;
import me.luizedgar.remomilo.data.Resource;
import me.luizedgar.remomilo.data.local.RemoMiloDatabase;
import me.luizedgar.remomilo.data.local.ResourcesDao;
import me.luizedgar.remomilo.utils.AppExecutors;
import me.luizedgar.remomilo.utils.FilterDialog;
import me.luizedgar.remomilo.utils.Snippets;
import me.luizedgar.remomilo.utils.VerticalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ResourcesFragment extends Fragment implements ResourcesContract.View, SearchView.OnQueryTextListener {

    private OnListFragmentInteractionListener mListener;

    @BindView(R.id.list)
    RecyclerView mRecyclerView;

    @BindView(R.id.message)
    TextView mMessageView;

    private ResourcesContract.Presenter mPresenter;

    private SearchView mSearchView;
    private ProgressDialog mLoadingProgress;
    private ProgressDialog mFilteringProgress;
    private String mCurrentQuery;

    public ResourcesFragment() {
    }

    public static ResourcesFragment newInstance() {
        ResourcesFragment fragment = new ResourcesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resource_list, container, false);

        ButterKnife.bind(this, view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.recicler_view_vertical_spacing)));
        mRecyclerView.setAdapter(new ResourceRecyclerViewAdapter(new ArrayList<Resource>(), mListener));

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.resources, menu);

        mSearchView = (SearchView) menu.findItem(R.id.search_action).getActionView();
        mSearchView.setOnQueryTextListener(this);
//        mSearchView.setQuery("", false);
//        mSearchView.setIconified(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.filter_action){
            showFilteringPopUpMenu();
        }else if (item.getItemId() == R.id.refresh_action){
            mPresenter.loadResources(true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            if (mLoadingProgress == null) {
                mLoadingProgress = Snippets.createProgressDialog(getContext(), R.string.loading_progress);
                mLoadingProgress.setCancelable(false);
            }
            if (!mLoadingProgress.isShowing()) {
                mLoadingProgress.show();
            }
        } else {
            if (mLoadingProgress != null && mLoadingProgress.isShowing()) {
                mLoadingProgress.dismiss();
                mLoadingProgress = null;
            }
        }
    }

    @Override
    public void setFilteringIndicator(boolean active) {
        if (active) {
            if (mFilteringProgress == null) {
                mFilteringProgress = Snippets.createProgressDialog(getContext(), R.string.filtering_progress);
                mFilteringProgress.setCancelable(false);
            }
            if (!mFilteringProgress.isShowing()) {
                mFilteringProgress.show();
            }
        } else {
            if (mFilteringProgress != null && mFilteringProgress.isShowing()) {
                mFilteringProgress.dismiss();
                mFilteringProgress = null;
            }
        }
    }

    @Override
    public void showResources(final List<Resource> resources) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRecyclerView.setAdapter(new ResourceRecyclerViewAdapter(resources, mListener));
            }
        });
    }

    @Override
    public void showResourceDetailsUi(String resourceId) {

    }

    @Override
    public void showLoadingTasksError() {
        Snippets.createSimpleMessageDialog(getContext(), R.string.error_loading_title, R.string.error_loading_message).show();
    }

    @Override
    public void showNoTasks() {
        mMessageView.setText(R.string.no_items);
    }

    @Override
    public void showFilteringPopUpMenu() {
        final AppExecutors appExecutors = new AppExecutors();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ResourcesDao resourcesDao = RemoMiloDatabase.getInstance(getContext()).resourceDao();

                final List<String> languages = resourcesDao.getLanguages();
                final List<String> modules = resourcesDao.getModules();

                languages.add(0, "Select…");
                modules.add(0, "Select…");

                appExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        DialogFragment newFragment = FilterDialog.newInstance(languages, modules, new FilterDialog.Listener() {
                            @Override
                            public void onFilter(String language, String module) {
                                mPresenter.setFiltering(mCurrentQuery, language, module);
                            }
                        });
                        newFragment.show(getChildFragmentManager(), "filter");
                    }
                });
            }
        };

        appExecutors.diskIO().execute(runnable);

    }

    @Override
    public void setPresenter(ResourcesContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mPresenter.setFiltering(query, null, null);
        mCurrentQuery = query;
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)){
            mPresenter.setFiltering("", null, null);
        }
        mCurrentQuery = newText;
        return false;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Resource resource);
    }
}
