package me.luizedgar.remomilo.resources;


import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import me.luizedgar.remomilo.data.Resource;
import me.luizedgar.remomilo.data.ResourcesDataSource;
import me.luizedgar.remomilo.data.ResourcesRepository;

/**
 * Created by luizfreitas on 23/08/2017.
 */

public class ResourcesPresenter implements ResourcesContract.Presenter {

    private ResourcesFragment mView;
    private ResourcesRepository mResourcesRepository;

    public ResourcesPresenter(ResourcesFragment resourcesFragment, ResourcesRepository resourcesRepository) {
        mView = resourcesFragment;
        mResourcesRepository = resourcesRepository;

        mView.setPresenter(this);
    }

    @Override
    public void start() {
        loadResources(false);
    }

    @Override
    public void loadResources(boolean forceUpdate) {
        mView.setLoadingIndicator(true);
        if (forceUpdate){
            mResourcesRepository.refreshResources();
        }
        mResourcesRepository.getResources(new ResourcesDataSource.LoadResourcesCallback() {
            @Override
            public void onResourcesLoaded(List<Resource> resources) {
                mView.showResources(resources);
                mView.setLoadingIndicator(false);
                Log.d("TESTE", "onResourcesLoaded LOCAL");
            }

            @Override
            public void onDataNotAvailable() {
                Log.d("TESTE", "onDataNotAvailable LOCAL");
                mResourcesRepository.fetchResourcesFromServer(new ResourcesDataSource.FetchResourcesFromNetworkCallback() {
                    @Override
                    public void onResourcesFetched(List<Resource> resources) {
                        mView.setLoadingIndicator(false);
                        mView.showResources(resources);
                        Log.d("TESTE", "onResourcesFetched SERVER");

                    }

                    @Override
                    public void onDataNotAvailable() {
                        Log.d("TESTE", "onDataNotAvailable SERVER 1");
                        mView.showNoTasks();
                        mView.setLoadingIndicator(false);
                    }
                });
            }
        });
    }

    @Override
    public void openResourceDetails(Resource requestedResource) {

    }

    @Override
    public void setFiltering(String query, String language, String module) {
        mView.setFilteringIndicator(true);
        ResourcesDataSource.LoadResourcesCallback loadResourcesCallback = new ResourcesDataSource.LoadResourcesCallback() {
            @Override
            public void onResourcesLoaded(List<Resource> resources) {
                mView.showResources(resources);
                mView.setFilteringIndicator(false);
            }

            @Override
            public void onDataNotAvailable() {
                mView.showNoTasks();
                mView.setFilteringIndicator(false);
            }
        };
        if (TextUtils.isEmpty(query) && TextUtils.isEmpty(language) && TextUtils.isEmpty(module)) {
            mResourcesRepository.getResources(loadResourcesCallback);
        } else {
            if (!TextUtils.isEmpty(query)){
                query = "%"+query+"%";
            }
            mResourcesRepository.getResourcesFilter(query, language, module, loadResourcesCallback);
        }
    }
}
