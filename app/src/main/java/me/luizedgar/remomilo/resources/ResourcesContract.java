package me.luizedgar.remomilo.resources;

import java.util.List;

import me.luizedgar.remomilo.data.Resource;
import me.luizedgar.remomilo.utils.BasePresenter;
import me.luizedgar.remomilo.utils.BaseView;

public interface ResourcesContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean active);

        void setFilteringIndicator(boolean active);

        void showResources(List<Resource> resources);

        void showResourceDetailsUi(String resourceId);

        void showLoadingTasksError();

        void showNoTasks();

        void showFilteringPopUpMenu();
    }

    interface Presenter extends BasePresenter {

        void loadResources(boolean forceUpdate);

        void openResourceDetails(Resource requestedResource);

        void setFiltering(String query, String language, String module);
    }
}