package me.luizedgar.remomilo.utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

import me.luizedgar.remomilo.R;

public class FilterDialog extends DialogFragment {

    private static Listener mListener;
    private List<String> mModules;
    private List<String> mLanguages;

    public interface Listener{
        void onFilter(String language, String module);
    }

    public static FilterDialog newInstance(List<String> languages, List<String> modules, Listener listener) {
        Bundle args = new Bundle();

        FilterDialog fragment = new FilterDialog();
        fragment.mLanguages = languages;
        fragment.mModules = modules;
        fragment.mListener = listener;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_filter, null);

        final Spinner languageSpinner = view.findViewById(R.id.language);
        languageSpinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mLanguages));
        final Spinner moduleSpinner = view.findViewById(R.id.module);
        moduleSpinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mModules));

        builder.setView(view).setMessage(R.string.filter_title)
               .setPositiveButton(R.string.filter_title, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null){
                            mListener.onFilter(languageSpinner.getSelectedItemPosition() != 0 ? ((String) languageSpinner.getSelectedItem()) : null, moduleSpinner.getSelectedItemPosition() != 0 ? ((String) moduleSpinner.getSelectedItem()) : null);
                        }
                   }
               })
               .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                   }
               });
        return builder.create();
    }
}