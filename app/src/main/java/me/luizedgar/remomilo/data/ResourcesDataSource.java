package me.luizedgar.remomilo.data;

import java.util.List;

public interface ResourcesDataSource {
    interface FetchResourcesFromNetworkCallback {
        void onResourcesFetched(List<Resource> resources);

        void onDataNotAvailable();
    }

    interface LoadResourcesCallback {
        void onResourcesLoaded(List<Resource> resources);

        void onDataNotAvailable();
    }

    interface GetResourceCallback {
        void onResourceLoaded(Resource resource);

        void onDataNotAvailable();
    }

    void getResources(LoadResourcesCallback callback);

    void getResourcesFilter(String query, String language, String module, LoadResourcesCallback callback);

    void getResource(String resourceId, GetResourceCallback callback);

    void saveResource(Resource resource);

    void refreshResources();

    void deleteAllResources();

}
