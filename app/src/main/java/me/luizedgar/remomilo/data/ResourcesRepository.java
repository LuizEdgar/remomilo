package me.luizedgar.remomilo.data;

import java.util.ArrayList;
import java.util.List;

import me.luizedgar.remomilo.data.remote.Api;
import me.luizedgar.remomilo.data.remote.ServerResourceWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResourcesRepository implements ResourcesDataSource {

    private static ResourcesRepository INSTANCE = null;

    private final Api.ApiClient mResourcesRemoteApi;

    private final ResourcesDataSource mResourcesLocalDataSource;

    boolean mLocalIsDirty = false;

    private ResourcesRepository(Api.ApiClient resourcesRemoteApi,
                                ResourcesDataSource resourcesLocalDataSource) {
        mResourcesRemoteApi = resourcesRemoteApi;
        mResourcesLocalDataSource = resourcesLocalDataSource;
    }

    public static ResourcesRepository getInstance(Api.ApiClient resourcesRemoteApi,
                                                  ResourcesDataSource resourcesLocalDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new ResourcesRepository(resourcesRemoteApi, resourcesLocalDataSource);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void getResources(final LoadResourcesCallback callback) {
        if (mLocalIsDirty) {
            callback.onDataNotAvailable();
        } else {
            mResourcesLocalDataSource.getResources(new LoadResourcesCallback() {
                @Override
                public void onResourcesLoaded(List<Resource> resources) {
                    callback.onResourcesLoaded(resources);
                }

                @Override
                public void onDataNotAvailable() {
                    callback.onDataNotAvailable();
                }
            });
        }
    }

    @Override
    public void getResourcesFilter(String query, String language, String module, final LoadResourcesCallback callback) {
        mResourcesLocalDataSource.getResourcesFilter(query, language, module, new LoadResourcesCallback() {
            @Override
            public void onResourcesLoaded(List<Resource> resources) {
                callback.onResourcesLoaded(resources);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void getResource(final String resourceId, final GetResourceCallback callback) {
        mResourcesLocalDataSource.getResource(resourceId, new GetResourceCallback() {
            @Override
            public void onResourceLoaded(Resource resource) {
                callback.onResourceLoaded(resource);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void saveResource(Resource resource) {
        mResourcesLocalDataSource.saveResource(resource);
    }

    @Override
    public void refreshResources() {
        mLocalIsDirty = true;
    }

    @Override
    public void deleteAllResources() {
        mResourcesLocalDataSource.deleteAllResources();
    }

    private List<Resource> refreshLocalDataSourceFromServer(List<ServerResourceWrapper> resourcesWrappers) {
        List<Resource> resources = new ArrayList<>();
        mResourcesLocalDataSource.deleteAllResources();
        for (ServerResourceWrapper resourceWrapper : resourcesWrappers) {
            Resource resource = resourceWrapper.getResource();
            mResourcesLocalDataSource.saveResource(resource);
            resources.add(resource);
        }
        return resources;
    }

    public void fetchResourcesFromServer(final FetchResourcesFromNetworkCallback callback){

        mResourcesRemoteApi.getResources().enqueue(new Callback<List<ServerResourceWrapper>>() {
            @Override
            public void onResponse(Call<List<ServerResourceWrapper>> call, Response<List<ServerResourceWrapper>> response) {
                callback.onResourcesFetched(refreshLocalDataSourceFromServer(response.body()));
            }

            @Override
            public void onFailure(Call<List<ServerResourceWrapper>> call, Throwable t) {
                callback.onDataNotAvailable();
            }
        });
    }

}