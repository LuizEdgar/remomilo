/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.luizedgar.remomilo.data.local;

import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.util.List;

import me.luizedgar.remomilo.data.Resource;
import me.luizedgar.remomilo.data.ResourcesDataSource;
import me.luizedgar.remomilo.utils.AppExecutors;

public class ResourcesLocalDataSource implements ResourcesDataSource {

    private static volatile ResourcesLocalDataSource INSTANCE;

    private ResourcesDao mResourcesDao;

    private AppExecutors mAppExecutors;

    private ResourcesLocalDataSource(AppExecutors appExecutors,
                                     ResourcesDao resourcesDao) {
        mAppExecutors = appExecutors;
        mResourcesDao = resourcesDao;
    }

    public static ResourcesLocalDataSource getInstance(AppExecutors appExecutors,
                                                       ResourcesDao resourcesDao) {
        if (INSTANCE == null) {
            synchronized (ResourcesLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ResourcesLocalDataSource(appExecutors, resourcesDao);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void getResources(final LoadResourcesCallback callback) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final List<Resource> resources = mResourcesDao.getResources();

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {

                        if (resources.isEmpty()) {
                            // This will be called if the table is new or just empty.
                            callback.onDataNotAvailable();
                        } else {
                            callback.onResourcesLoaded(resources);
                        }
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void getResourcesFilter(final String query, final String language, final String module, final LoadResourcesCallback callback) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                final List<Resource> resources;


                if (!TextUtils.isEmpty(query)) {
                    if (!TextUtils.isEmpty(language) && !TextUtils.isEmpty(module)) {
                        resources = mResourcesDao.getResourcesQLM(query, language, module);
                    } else if (!TextUtils.isEmpty(language)) {
                        resources = mResourcesDao.getResourcesQL(query, language);
                    } else if (!TextUtils.isEmpty(module)) {
                        resources = mResourcesDao.getResourcesQM(query, module);
                    } else {
                        resources = mResourcesDao.getResourcesQ(query);
                    }
                } else if (!TextUtils.isEmpty(language)) {
                    if (!TextUtils.isEmpty(module)) {
                        resources = mResourcesDao.getResourcesLM(language, module);
                    } else {
                        resources = mResourcesDao.getResourcesL(language);
                    }
                } else if (!TextUtils.isEmpty(module)) {
                    resources = mResourcesDao.getResourcesM(module);
                } else {
                    resources = mResourcesDao.getResources();
                }

                mAppExecutors.mainThread().

                        execute(new Runnable() {
                            @Override
                            public void run() {

                                if (resources.isEmpty()) {
                                    // This will be called if the table is new or just empty.
                                    callback.onDataNotAvailable();
                                } else {
                                    callback.onResourcesLoaded(resources);
                                }
                            }
                        });
            }
        };

        mAppExecutors.diskIO().

                execute(runnable);

    }

    @Override
    public void getResource(final String resourceId, final GetResourceCallback callback) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Resource resource = mResourcesDao.getResourceById(resourceId);

                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (resource != null) {
                            callback.onResourceLoaded(resource);
                        } else {
                            callback.onDataNotAvailable();
                        }
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void saveResource(final Resource resource) {
        Runnable saveRunnable = new Runnable() {
            @Override
            public void run() {
                mResourcesDao.insertResource(resource);
            }
        };
        mAppExecutors.diskIO().execute(saveRunnable);
    }

    @Override
    public void refreshResources() {
    }

    @Override
    public void deleteAllResources() {
        Runnable deleteRunnable = new Runnable() {
            @Override
            public void run() {
                mResourcesDao.deleteResources();
            }
        };

        mAppExecutors.diskIO().execute(deleteRunnable);
    }

    @VisibleForTesting
    static void clearInstance() {
        INSTANCE = null;
    }
}
