/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.luizedgar.remomilo.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import java.util.List;

import me.luizedgar.remomilo.data.Resource;

@Dao
public interface ResourcesDao {

    @Query("SELECT * FROM Resources")
    List<Resource> getResources();

    @Query("SELECT * FROM Resources WHERE id LIKE :query")
    List<Resource> getResourcesQ(String query);

    @Query("SELECT * FROM Resources WHERE language_id = :language")
    List<Resource> getResourcesL(String language);

    @Query("SELECT * FROM Resources WHERE module_id = :module")
    List<Resource> getResourcesM(String module);

    @Query("SELECT * FROM Resources WHERE id LIKE :query AND language_id = :language")
    List<Resource> getResourcesQL(String query, String language);

    @Query("SELECT * FROM Resources WHERE id LIKE :query AND module_id = :module")
    List<Resource> getResourcesQM(String query, String module);

    @Query("SELECT * FROM Resources WHERE language_id = :language AND module_id = :module")
    List<Resource> getResourcesLM(String language, String module);

    @Query("SELECT * FROM Resources WHERE id LIKE :query AND language_id = :language AND module_id = :module")
    List<Resource> getResourcesQLM(String query, String language, String module);

    @Query("SELECT DISTINCT language_id FROM Resources")
    List<String> getLanguages();

    @Query("SELECT DISTINCT module_id FROM Resources")
    List<String> getModules();

    @Query("SELECT * FROM Resources WHERE id = :resourceId")
    Resource getResourceById(String resourceId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertResource(Resource resource);

    @Update
    int updateResource(Resource resource);

    @Query("DELETE FROM Resources WHERE id = :resourceId")
    int deleteResourceById(String resourceId);

    @Query("DELETE FROM Resources")
    void deleteResources();
}
