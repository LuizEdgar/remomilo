package me.luizedgar.remomilo.data.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import me.luizedgar.remomilo.data.Resource;

/**
 * Created by luizfreitas on 24/08/2017.
 */

public class ServerResourceWrapper {

    @Expose
    private Resource resource;

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
