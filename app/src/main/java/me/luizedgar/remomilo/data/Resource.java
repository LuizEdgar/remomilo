package me.luizedgar.remomilo.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by luizfreitas on 23/08/2017.
 */

@Entity(tableName = "resources", primaryKeys = {"id", "language_id", "module_id"})
public class Resource {

    @Expose
    @SerializedName("resource_id")
    @ColumnInfo(name = "id")
    private String id;

    @Expose
    @SerializedName("module_id")
    @ColumnInfo(name = "module_id")
    private String moduleId;

    @Expose
    @ColumnInfo(name = "value")
    private String value;

    @Expose
    @SerializedName("language_id")
    @ColumnInfo(name = "language_id")
    private String languageId;

    @Expose
    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")

    private Date createdAt;

    @Expose
    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    private Date updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
